import { component$ } from "@builder.io/qwik";
import styles from "./navbar.module.css";
import { Link } from "@builder.io/qwik-city";

export default component$(() => {
  return (
    <header class="p-10  max-w-5xl mx-auto">
      <div class="flex justify-between">
        <div class={styles.logo}>
          <Link href="/" title="qwik">
            logo
          </Link>
        </div>
        <ul class="flex justify-center gap-6">
          <Link href="/login" title="login">
            Login
          </Link>
          <Link href="/dashboard" title="dashboard">
            dashboard
          </Link>
          <Link href="/pokemons/list-ssr/" title="SSR">
            SSR
          </Link>
          <Link href="/pokemons/list-client/" title="Client">
            client
          </Link>
        </ul>
      </div>
    </header>
  );
});
