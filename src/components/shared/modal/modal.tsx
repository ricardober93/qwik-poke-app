import {
  type PropFunction,
  Slot,
  component$,
  useStylesScoped$,
} from "@builder.io/qwik";
import ModalStyles from "./modal.css?inline";

interface ModalProps {
  showModal: boolean;
  closeModal: PropFunction<() => void>;
  persistent?: boolean;
  size?: "sm" | "md" | "lg";
}

export const Modal = component$(({
  showModal,
  closeModal,
  persistent = false,
  size = "md",
}: ModalProps) => {
  useStylesScoped$(ModalStyles);

  return (
      <div
      
      onClick$={(event) => {
        const idModal = (event.target as HTMLDivElement).id;
        if (idModal === "modal" && !persistent) {
          closeModal();
        }
      }}
      class={showModal ? "modal-background" : "hidden"}
      id="modal"
    >
      <div class={[{
        "modal-sm": size === "sm",
        "modal-md": size === "md",
        "modal-lg": size === "lg",
        },"modal-content"]}>
        <div class="mt-3 text-center">
          <h3 class="modal-title">
            {" "}
            <Slot name="title" />{" "}
          </h3>

          <div class="mt-2 px-7 py-3">
            <div class="modal-content-text">
              <Slot name="content"></Slot>
            </div>
            <p class="text-black">
              preguntandole a chatGPT
            </p>
          </div>

          <div class="items-center px-4 py-3">
            <button onClick$={closeModal} id="ok-btn" class="modal-button">
              Cerrar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
});
