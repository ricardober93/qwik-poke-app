import { component$, useComputed$ } from "@builder.io/qwik";

export interface PokemonImagesProps {
  count: number;
  zise?: number;
  height?: number;
  backImage?: boolean;
  isHidden?: boolean;
}

export const PokemonImages = component$<PokemonImagesProps>(({
  count,  
  backImage= false,
  height = 200, 
  zise = 200, 
  isHidden = true
}) => {


  const urlImage = useComputed$(() => {
    return backImage ? 
    `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/${count}.png` 
    : `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${count}.png`;
  }
  );

  return (
    <img 
      class={[{
        "mx-auto": true,
        "brightness-0": !isHidden,
      }, 'transition-all duration-300']}
      alt="pokemon"
      width={zise}
      height={height}
      src={urlImage.value}
       />

      
  );
});