import {
  Slot,
  component$,
  useContextProvider,
  useStore,
  useVisibleTask$,
} from "@builder.io/qwik";
import {
  type PokemonListState,
  type PokemonGameState,
  PokemonGameStateContext,
  PokemonListContext,
} from "~/context";

export const PokemonProvider = component$(() => {
  const pokemonGame = useStore<PokemonGameState>({
    isBackImage: false,
    isVisiblePokemon: false,
    pokemonId: 1,
  });

  const pokemonList = useStore<PokemonListState>({
    pokemons: [],
    isloading: false,
    currentPage: 0,
    isLastPage: false,
  });

  useContextProvider(PokemonGameStateContext, pokemonGame);
  useContextProvider(PokemonListContext, pokemonList);

useVisibleTask$(() => {
 const data = localStorage.getItem('pokemonGame');
 if (data !== null) {
     const parsedData = JSON.parse(data) as PokemonGameState;
     pokemonGame.pokemonId = parsedData.pokemonId;
     pokemonGame.isBackImage = parsedData.isBackImage;
     pokemonGame.isVisiblePokemon = parsedData.isVisiblePokemon;
 }
 
});

  useVisibleTask$(({track}) => {

    track(() => [pokemonGame.pokemonId, pokemonGame.isBackImage, pokemonGame.isVisiblePokemon]);

    localStorage.setItem("pokemonGame", JSON.stringify(pokemonGame));
    
   });

  return <Slot />;
});
