import { $, useComputed$, useContext } from "@builder.io/qwik";
import { PokemonGameStateContext } from "~/context";

export const usePokemonGame = () => {
 
    const pokemonGame = useContext(PokemonGameStateContext);

    const changeId = $(( operator: "increment" | "decrement") => {
    
        if (operator === "increment") {
          pokemonGame.pokemonId = pokemonGame.pokemonId +1;
        }
    
        if (operator === "decrement") {
          pokemonGame.pokemonId = pokemonGame.pokemonId - 1;
        }
    });


    const toggleBackImage = $(() => {
        pokemonGame.isBackImage = !pokemonGame.isBackImage;
    });

    const toggleVisible = $(() => {
        pokemonGame.isVisiblePokemon = !pokemonGame.isVisiblePokemon;
    });
    
    return {
        
        id: useComputed$( () => pokemonGame.pokemonId ),
        showBackImage: useComputed$( () =>  pokemonGame.isBackImage ),
        showPokemon: useComputed$(() => pokemonGame.isVisiblePokemon),
        changeId: changeId,
        toggleBackImage: toggleBackImage,
        toggleVisible: toggleVisible,
    };
};
