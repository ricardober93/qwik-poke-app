import { $, component$, useContext, useOnDocument, useTask$, } from "@builder.io/qwik";
import { useLocation, type DocumentHead } from "@builder.io/qwik-city";
import { PokemonImages } from "~/components/pokemon/pokemon-images.tsx/pokemon-images";
import { PokemonListContext } from "~/context";
import { getPokemons } from "~/helpers/getPokemons";
import type { SmallPokemon } from "~/interface";


export default component$(() => {

  const pokemonState = useContext(PokemonListContext);

  // useVisibleTask$(async ({ track }) => {
  //   track(() => pokemonState.currentPage);
  //   const pokemon =  await getPokemons( {offset: pokemonState.currentPage * 10} );
  //   pokemonState.pokemons = pokemon;
  // });

  useTask$(async ({ track }) => {
    track(() => pokemonState.currentPage);
    pokemonState.isloading = true;
    
    const pokemon =  await getPokemons( {offset: pokemonState.currentPage * 10, limit: 50} );
    pokemonState.pokemons = [...pokemonState.pokemons,...pokemon];

    if (pokemonState.pokemons.length >= 1118) {
      pokemonState.isLastPage = true;
    }

    pokemonState.isloading = false;
  });


  useOnDocument("scroll", $(() => {
    const maxScroll = document.body.scrollHeight - window.innerHeight;
    const currentScroll = window.scrollY;
    if (currentScroll >= maxScroll && !pokemonState.isloading && !pokemonState.isLastPage) {
      pokemonState.currentPage++;
    }
  })
  );

  const location = useLocation();
  return (
    <main class="flex flex-col justify-center items-center">
      <div class="flex flex-col">
        <span class="my-5 text-5xl">status</span>
        <span class=""> Pagina Actual: { pokemonState.currentPage } </span>
        <span class="">
          {" "}
          Esta cargando: {location.isNavigating ? "si" : " no"}{" "}
        </span>
      </div>

      <div class="mt-5 flex gap-5">
        {/* <button 
        onClick$={() => {  pokemonState.currentPage = pokemonState.currentPage - 1; }}
        class="bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded">
          {" "}
          anterior
        </button> */}
        <button 
          onClick$={() => {  pokemonState.currentPage = pokemonState.currentPage + 1; }}
        class="bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded">
          {" "}
          siguiente
        </button>
      </div>

      <div class=" grid grid-cols-6 mt-5">
        {pokemonState.pokemons.map((pokemon: SmallPokemon, i: number) => (
          <div
            key={`${pokemon.id}-${pokemon.name}-${i}`}
            class="flex flex-col m-4 justify-center items-center"
          >
            <PokemonImages count={Number(pokemon.id)} />

            <span> {pokemon.name} </span>
          </div>
        ))}
      </div>
    </main>
  );
});

export const head: DocumentHead = {
  title: "Client",
  meta: [
    {
      name: "description",
      content: "Qwik site description",
    },
  ],
};
