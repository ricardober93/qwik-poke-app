import { $, component$, useComputed$, useSignal, useStore } from "@builder.io/qwik";
import {
  routeLoader$,
  type DocumentHead,
  useLocation,
  Link,
} from "@builder.io/qwik-city";
import { PokemonImages } from "~/components/pokemon/pokemon-images.tsx/pokemon-images";
import { Modal } from "~/components/shared";
import { getPokemons } from "~/helpers/getPokemons";
import type { SmallPokemon } from "~/interface/";

export const usePokemon = routeLoader$<SmallPokemon[]>(
  async ({ query, redirect, pathname }) => {
    const offset = Number(query.get("offset"));
    if (isNaN(offset)) redirect(301, pathname);

    if (offset < 0) redirect(301, pathname);

    const pokemon = getPokemons({ offset });

    return pokemon;
  }
);

export default component$(() => {
  const pokemons = usePokemon();
  const location = useLocation();

  const pokemon = useStore({
    id: 0,
    name: "",
  });
  const modalVisible = useSignal(false);

  const selecedPokemon = $((id: string, name:string) => {
    modalVisible.value = true;
    pokemon.id = Number(id);
    pokemon.name = name;
  });


  const closeModal = $(() => {

    modalVisible.value = false;
  });

  const currentOffset = useComputed$(() => {
    const offset = location.url.searchParams.get("offset");
    return Number(offset);
  });

  return (
    <main class="flex flex-col justify-center items-center">
      <div class="flex flex-col">
        <span class="my-5 text-5xl">status</span>
        <span class=""> offset: {currentOffset} </span>
        <span class="">
          {" "}
          Esta pagina esta cargando: {location.isNavigating ? "si" : " no"}{" "}
        </span>
      </div>

      <div class="mt-5 flex gap-5">
        <Link
          href={`/pokemons/list-ssr?offset=${currentOffset.value - 10}`}
          class="bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded"
        >
          {" "}
          anterior
        </Link>
        <Link
          href={`/pokemons/list-ssr?offset=${currentOffset.value + 10}`}
          class="bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded"
        >
          {" "}
          siguiente
        </Link>
      </div>

      <div class=" grid grid-cols-6 mt-5">
        {pokemons.value.map((pokemon: SmallPokemon) => (
          <div
            key={pokemon.name}
            class="flex flex-col m-4 justify-center items-center"
            onClick$={() => selecedPokemon(pokemon.id, pokemon.name)}
          >
            <PokemonImages count={Number(pokemon.id)} />

            <span> {pokemon.name} </span>
          </div>
        ))}
      </div>

      <Modal persistent size="md" closeModal={closeModal} showModal={modalVisible.value}>
        <div q:slot="title">
          <h1>{ pokemon.name } </h1>
        </div>
        <PokemonImages q:slot="content" count={pokemon.id} />
      </Modal>
    </main>
  );
});

export const head: DocumentHead = {
  title: "SSR",
  meta: [
    {
      name: "description",
      content: "Qwik site description",
    },
  ],
};
