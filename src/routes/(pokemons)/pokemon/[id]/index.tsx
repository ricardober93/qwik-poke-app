import { component$ } from "@builder.io/qwik";
import { routeLoader$ } from "@builder.io/qwik-city";
import { PokemonImages } from "~/components/pokemon/pokemon-images.tsx/pokemon-images";
import { usePokemonGame } from "~/hooks/usePokemonGame";

export const usePokemonId = routeLoader$<number>(({ params, redirect }) => {
  const id = Number(params.id);

  if (isNaN(id) || id < 1 || id > 898) {
    redirect(301, "/");
  }

  return id;
});

export default component$(() => {
  const pokemonId = usePokemonId();

  const {
    showBackImage,
    showPokemon,
    toggleBackImage,
    toggleVisible,
  } = usePokemonGame();
  return (
    <main>
      <PokemonImages count={pokemonId.value} backImage={showBackImage.value} isHidden={showPokemon.value} />

      <div
       class="   w-full flex justify-center items-center gap-5">
      <button
          class="   bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded"
          onClick$={ toggleVisible}
        >
          revelar
        </button>
        <button
          class="   bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded"
          onClick$={toggleBackImage}
        >
          voltear
        </button>

    </div>
    </main>
  );
});
