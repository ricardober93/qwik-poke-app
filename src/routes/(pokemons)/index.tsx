import { $, component$ } from "@builder.io/qwik";
import { useNavigate, type DocumentHead } from "@builder.io/qwik-city";
import { PokemonImages } from "~/components/pokemon/pokemon-images.tsx/pokemon-images";
import { usePokemonGame } from "~/hooks/usePokemonGame";

export default component$(() => {
  const navigate = useNavigate();

  const {
    id,
    showBackImage,
    showPokemon,
    changeId,
    toggleBackImage,
    toggleVisible,
  } = usePokemonGame();

  const goPokemon = $((id: number) => {
    navigate(`/pokemon/${id}`);
  });

  return (
    <main>
      <h1 class="text-3xl text-center"> Buscador Simple </h1>
      <h2 class="text-9xl text-center "> {id.value} </h2>

      <article onClick$={() => goPokemon(id.value)}>
        <PokemonImages
          count={id.value}
          isHidden={showPokemon.value}
          backImage={showBackImage.value}
        />
      </article>

      <div class="flex justify-center gap-1">
        <button
          class=" bg-white text-purple-950 py-2 px-4 border border-purple-950 rounded  "
          onClick$={() => changeId("decrement")}
        >
          -
        </button>
        <button
          class=" bg-white text-purple-950 py-2 px-4 border border-purple-950 rounded  "
          onClick$={() => changeId("increment")}
        >
          +
        </button>
        <button
          class="   bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded"
          onClick$={ toggleVisible}
        >
          revelar
        </button>
        <button
          class="   bg-purple-900 text-white py-2 px-4 border border-purple-950 rounded"
          onClick$={toggleBackImage}
        >
          voltear
        </button>
      </div>
    </main>
  );
});

export const head: DocumentHead = {
  title: "Welcome to Qwik",
  meta: [
    {
      name: "description",
      content: "Qwik site description",
    },
  ],
};
