import { component$, useStylesScoped$ } from "@builder.io/qwik";

import styles from "./login.css?inline";
import { Form, routeAction$, zod$, z } from "@builder.io/qwik-city";

export const useLoginAction = routeAction$(
  (data, { cookie, redirect }) => {
    const { email, password } = data;

    if (email === "" || password === "") {
      return {
        success: false,
        message: "Email and password are required",
      };
    }
    if (email === "ricardo@gmail.com" && password === "123456") {
      cookie.set("jwt", "jwt-token", {
        secure: true,
        path: "/",
      });
      redirect(303, "/");
      return {
        success: true,
        message: "Login successful",
        jwt: "jwt-token",
      };
    }
  },
  zod$({
    email: z.string().email("Invalid email"),
    password: z.string().min(6, "Password must be at least 6 characters"),
  })
);

export default component$(() => {
  useStylesScoped$(styles);

  const loginAction = useLoginAction();

  return (
    <Form action={loginAction} class="login-form mt-10">
      <div class="relative">
        <input name="email" type="text" placeholder="Email address" />
        <label for="email">Email Address</label>
      </div>
      <div class="relative">
        <input name="password" type="password" placeholder="Password" />
        <label for="password">Password</label>
      </div>
      <div class="relative">
        <button type="submit">Ingresar</button>
      </div>
    </Form>
  );
});
