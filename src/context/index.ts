import { createContextId } from "@builder.io/qwik";
import type { SmallPokemon } from "~/interface";

export interface PokemonGameState {
  pokemonId: number;
  isBackImage: boolean;
  isVisiblePokemon: boolean;
}

export interface PokemonListState {
  pokemons: SmallPokemon[];
  currentPage: number;
  isloading: boolean;
  isLastPage: boolean;
}

export const PokemonGameStateContext =
  createContextId<PokemonGameState>("PokemonGameState");



export const PokemonListContext =
createContextId<PokemonListState>("PokemonListContext");
