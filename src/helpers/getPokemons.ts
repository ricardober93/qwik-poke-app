import type { ResponsePokemon, SmallPokemon } from "~/interface";

export const getPokemons = async ({
  limit = 10,
  offset = 0
}: {
    limit?: number;
    offset?: number;
}): Promise<SmallPokemon[]> => {
  const response = await fetch(
    `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`
  );
  const data = (await response.json()) as ResponsePokemon;


  return data.results.map((pokemon) => {
    const urlSplit = pokemon.url.split("/");
    const id = urlSplit.at(-2)!;

    return {
      id,
      name: pokemon.name,
    };
  });
};
